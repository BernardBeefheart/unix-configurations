# You can put files here to add functionality separated per file, which
# will be ignored by git.
# Files on the custom/ directory will be automatically loaded by the init
# script, in alphabetical order.

# For example: add yourself some shortcuts to projects you often work on.
#
# brainstormr=~/Projects/development/planetargon/brainstormr
# cd $brainstormr
#

# ZSH configuration (from grml's zshrc)
setopt append_history       # append history list to the history file (important for multiple parallel zsh sessions!)
setopt SHARE_HISTORY		# import new commands from the history file also in other zsh-session
setopt extended_history     # save each command's beginning timestamp and the duration to the history file
setopt histignorealldups	# If  a  new  command  line being added to the history
                            # list duplicates an older one, the older command is removed from the list
setopt histignorespace      # remove command lines from the history list when
                            # the first character on the line is a space
setopt nohup                # and don't kill them, either


# we need 256 colors for a nice mc
export TERM=xterm-256color

# bof...
[ -f /etc/timezone ] && export TZ=$(cat /etc/timezone)

# aliases
alias mc="mc --skin xoria256"
alias sumc="sudo mc --skin xoria256"
