#
# 00-kernel.zsh
# basic functions without knowledge of anything else
# than fresh environment


# add each arguments at the end of $PATH
# [tested]
function addToPath () {
	while [ $# -ne 0 ]
	do
		echo $PATH | grep $1 > /dev/null || export PATH=$PATH:$1
		shift
	done
}

# add each arguments at the beginning of $PATH
# [tested]
function pushToPath () {
	while [ $# -ne 0 ]
	do
		echo $PATH | grep $1 > /dev/null || export PATH=$1:$PATH
		shift
	done
}

# example :
# run xterm -e $(which bash)
# runs xterm in background, with no attaches from current terminal
# [tested]
function run() {
	[ $# -eq 0 ] && echo "run tool arguments ..." && return 1
	tool=$1
	shift
	nohup $tool $@ &> /dev/null &
    return 0
}


echo "PATH is $PATH"
