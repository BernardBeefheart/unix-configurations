# Site specific commands
#
export MANPATH=/usr/share/man:/usr/local/man:/usr/local/openjdk8/man:$MANPATH
export MANPATH=$MANPATH:/usr/local/lib/swipl-7.2.3/xpce/man:/usr/local/lib/perl5/5.20/perl/man:/usr/local/lib/node_modules/npm/man

pushToPath /usr/local/bin /usr/perso/bin ~/bin /bin

alias komodo="nohup ~/bin/komodo &> /dev/null &"
