" ======================================================================
" load-vundle.vim
" reload with:
" gvim +PluginInstall +qall
" ======================================================================
" pour Vundle
" pas de compatibilité
set nocompatible              " be iMproved, required

filetype off                  " required

" set the runtime path to include Vundle and initialize
" set rtp+=~/.vim/bundle/Vundle.vim
set rtp+=~/git/unix-configurations/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" Plugin fsharp:
Plugin 'kongo2002/fsharp-vim'

" Plugin git
Plugin 'tpope/vim-fugitive'
Plugin 'airblade/vim-gitgutter'

" pour plyml
Plugin 'vim-scripts/sml_polyml.vim'

" ligne de status
Plugin 'bling/vim-airline'

" javascript
Plugin 'pangloss/vim-javascript'

" ----------------------------------------------------------------------
"  pour un IDE très personnalisé

Plugin 'scrooloose/nerdtree'
Plugin 'fholgado/minibufexpl.vim'
Plugin 'majutsushi/tagbar'
Plugin 'ervandew/supertab'
Plugin 'Raimondi/delimitMate'

Plugin 'amiorin/vim-project'

Plugin 'groenewege/vim-less'
Plugin 'scrooloose/syntastic'
Plugin 'Lokaltog/vim-distinguished'
Plugin 'othree/html5.vim'
"
" ----------------------------------------------------------------------

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

" fin de Vundle
" ======================================================================

