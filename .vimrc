" .vimrc

" source ~/.vim/load-vundle.vim

" numéros de ligne
set nu

" coloration, indentation
syn  on
set  syntax =on
filetype  indent plugin on

" parenthèses correspondantes
set  showmatch

" tabulations
set  tabstop =4
set  shiftwidth =4
set  softtabstop =4

" recherche incrémentale
set  incsearch

" affiche la ligne du curseur
set  cursorline

" No annoying sound on errors
set noerrorbells
set novisualbell
set t_vb=
set tm=500

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colors and Fonts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Enable syntax highlighting
syntax enable

colorscheme desert
set background=dark

" Set extra options when running in GUI mode
if has("gui_running")
    set guioptions-=T
    set guioptions+=e
    set t_Co=256
    set guitablabel=%M\ %t
endif

" Set utf8 as standard encoding and en_US as the standard language
set encoding=utf8

" Use Unix as the standard file type
set ffs=unix,dos,mac

""""""""""""""""""""""""""""""
" => Status line
""""""""""""""""""""""""""""""
" " Always show the status line
set laststatus=2
" 
" " Format the status line
" set statusline=\ %{fugitive#statusline()}%F%m%r%h\ %w\ \ CWD:\ %r%{getcwd()}%h\ \ \ Line:\ %l
" 
" " Returns true if paste mode is enabled
" function! HasPaste()
"     if &paste
"         return 'PASTE MODE  '
"     en
"     return ''
" endfunction
"
" utilise le plugin airline
" let g:airline#extensions#tabline#enabled = 1

" markdown for .md files
let g:vim_markdown_folding_disabled=1

" ----------------------------------------------------------------------
" vim-project
let g:project_use_nerdtree = 1
set rtp+=~/.vim/bundle/vim-project/
" custom starting path
" call project#rc("~/git")

" Project 'ml-games/fsharp/fibo', 'FSharp_fibo'
" Project 'ml-games/remember', 'SML Remember'

" ----------------------------------------------------------------------
if v:version > 703 || v:version == 703 && has("patch541")
  set formatoptions+=j " Delete comment character when joining commented lines
endi

" gitgutter : https://github.com/airblade/vim-gitgutter
set shell=/bin/bash
" ----------------------------------------------------------------------
" for chicken syntax :
" let g:is_chicken = 1
let g:is_r7rs = 1
" ----------------------------------------------------------------------
"  pour syntactic
set statusline+=%#warningmsg#
"" set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" ----------------------------------------------------------------------
" COBOL
autocmd BufNewFile  *.cob      set ignorecase
autocmd BufNewFile  *.cob      set infercase
autocmd BufNewFile  *.cob      set virtualedit=all
autocmd BufNewFile  *.cob      set complete=k~/.vim/ocreserved.lis
autocmd BufNewFile  *.cbl      set ignorecase
autocmd BufNewFile  *.cbl      set infercase
autocmd BufNewFile  *.cbl      set virtualedit=all
autocmd BufNewFile  *.cbl      set complete=k~/.vim/ocreserved.lis


