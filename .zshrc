# Filename:      /etc/zsh/zshrc
# Purpose:       config file for zsh (z shell)
# Authors:       grml-team (grml.org), (c) Michael Prokop <mika@grml.org>
# Bug-Reports:   see http://grml.org/bugs/
# License:       This file is licensed under the GPL v2.
################################################################################
# This file is sourced only for interactive shells. It
# should contain commands to set up aliases, functions,
# options, key bindings, etc.
#
# Global Order: zshenv, zprofile, zshrc, zlogin
################################################################################
#

export _os=$(uname)
export _haslimit=1

config_dir=$HOME/unix-configurations

[ -f $config_dir/.zshrc.common ] && source $config_dir/.zshrc.common

case $_os in
    FreeBSD*)
        source $config_dir/.zshrc.BSD
        ;;
    SunOS*)
        source $config_dir/.zshrc.SunOS
        ;;
    Linux*)
        source $config_dir/.zshrc.Linux
        ;;
    CYGWIN*|cygwin*)
		_haslimit=0
        source $config_dir/.zshrc.Cygwin
        ;;
    *)
        source $config_dir/.zshrc.None
        ;;
esac

